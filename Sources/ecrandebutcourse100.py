#importation des modules
from tkinter import *
from PIL import Image, ImageTk

#creation de la fenetre 'screen'

screen = Tk()
screen.geometry("1100x800")
screen.title("Jogging Odyssey")

#### mise en place des variables globales

#numéro de l'image courante de coureuse.gif affichée (player)
current_frame2 = 0

# compteur de distance parcourue
distance = 0

#derniere jambe utilisée pour avancer prend les valeurs "left" ou "right"
last = ""#if level=="easy"
pas = 20 # vitesse du bot



# liste qui informe si les 3 lignes d'arrivée sont prêtes
finish = [False, False, False]

### définition des coordonnées

#dictionnaire des coordonnées des lignes line_coord[ligne n] renvoie [x,y] les coordonnées de la ligne
line_coord = {"line_1": [500, 395], # coordonnées de la ligne 1
              "line_2": [300, 460], # coordonnées de la ligne 2
              "line_3": [100, 525]  # coordonnées de la ligne 3
              }

#coordonnées du joueur
x_pos = 200 
x_bot = 0 

#coordonnées du stade en arrière plan
ystade = 300
xstade = 582

###variables de gestion du temps

# chrono=[heure, minute, seconde, centième de seconde
chrono=[0,0,0,0]
#variable pour arreter le chrono
arret = True



###gestion des images importées

#image d'arrière plan (stade et gradins)
img = Image.open("stade.jpg")
bg = ImageTk.PhotoImage(img)

#image de victoire si le joueur gagne
img = Image.open("medaille_or.png")
winner = ImageTk.PhotoImage(img)

#image de piste d'athlétisme
img = Image.open("coursepiste.jpg")
piste = ImageTk.PhotoImage(img)


# fonctions de gestions d'images

def load_gif_frames(gif_path: str) -> list:
    """
    charge les images composant gif_path
    retourne une liste des images
    """
    gif = Image.open(gif_path)#ouverture du gif
    images = []

    for i in range(gif.n_frames):# copie de chaque image du gif
        img = gif.copy()
        img.save("course" + str(i) + ".png")
        images.append(ImageTk.PhotoImage(img))
        if i < gif.n_frames - 1:
            gif.seek(gif.tell() + 1)
    return images#on renvoie la liste d'images

### fonctions de temps
def tim():
    """
    cette procédure actualise du chronomètre
    """
    global arret
    if arret == True:#s'arrete si le jeu est terminé
        mylabel.after(10, update)

def update():
    """
    cette procédure affiche le chronomètre
    """
    global chrono#prend le chronomètre global
    #variables d'affichage des 0 supplémentaires
    O_cs = ""
    O_s = ""
    O_m = ""
    chrono[3]+=1 #ajoute une centième de seconde au chronomètre
    if chrono[3] == 100:#passe de 100 centièmes de seconde à 1 seconde 
        chrono[3] = 0
        chrono[2] += 1
    if chrono[2] == 60:#passe de 60 s à 1min
        chrono[2] = 0
        chrono[1] += 1
    if chrono[1] == 60:#passe de 60 min à 1h
        chrono[1] = 0
        chrono[0] += 1
    if chrono[3] < 10:
        O_cs = "0"
    if chrono[2] < 10:
        O_s = "0"
    if chrono[1] < 10:
        O_m = "0"
    #affichage du chronomètre
    mylabel.config(text="0" + str(chrono[0]) +" : " + O_m + str(chrono[1]) +
                   " : " + O_s + str(chrono[2]) +
                   " : " + O_cs + str(chrono[3]),
                   font=('Arial', 33))
    tim()

def arreter():
    """ cette procédure indique d'arreter le chronomètre"""
    global arret
    arret = False

def save():
    """cette fonction sauvegarde le contenu du chronomètre"""
    global chrono
    return (str(chrono[0]) + " : " + str(chrono[1]) + " : " + str(chrono[2]) + " : " + str(chrono[3]))


# fonctions de mouvement

def animate_bot(canvas, gif_frames, current_frame, bot):
    """fonction qui anime le bot en enlevant la précedante image et
    en affichant celle d'après
    cette fonction sert aussi à actualiser la position du bot"""
    global pas, x_bot
    canvas.delete(bot)#suppression de l'ancienne image du robot
    bot = canvas.create_image(x_bot, 370, image=gif_frames[current_frame])#création de la nouvelle image d'animation
    current_frame = (current_frame + 1) % len(gif_frames)#actualisation de l'image 
    x_bot += pas#actualisation des coordonnées du robot
    if pas > 20:#limite du pas à 20
        pas = 20
    canvas.coords(bot, x_bot, 360)
    screen.after(100, animate_bot, canvas, gif_frames, current_frame, bot)

def move_right(event):
    """
    cette fonction fait avancer d'un pas de la droite le joueur, fait bouger les lignes, le joueur et le 
    stade pour donner une illusion de mouvement et renvoie la distance parcourue et les coordonnées du robot
    """
    global x_pos, coureuse1, coureuse2, line_coord, lines, distance, last, x_bot, current_frame2
    if last != "right":#si la jambe bougée precédement est la gauche (pas de cloche pied)
        last = "right"#actualisation de la dernière jambe
        distance += 1#actualisation de la distance parcourue
        x_pos += 2  # Ajuster la valeur pour contrôler la vitesse de déplacement
        x_bot -= 20 #Ajuster la valeur pour varier la difficulté

        canvas.itemconfigure(coureuse2, state='hidden')#on cache l'image gauche
        canvas.itemconfigure(coureuse1, state='normal')#on affiche l'image droite
        canvas.coords(coureuse1, x_pos, 500)
        for i in range(1, 4):#on actualise la position des lignes
            line_coord["line_" + str(i)][0] = move_line(i - 1, line_coord["line_" + str(i)][0],
                                                        line_coord["line_" + str(i)][1])

    return distance, x_bot

def move_left(event):
    """
    cette fonction fait avancer d'un pas de la gauche le joueur, fait bouger les lignes, le joueur et le 
    stade pour donner une illusion de mouvement et renvoie la distance parcourue et les coordonnées du robot
    """
    global x_pos, coureuse1, coureuse2, line_coord, lines, distance, last, x_bot, current_frame2, canvas
    if last != "left":#si la jambe bougée precédement est la gauche (pas de cloche pied)
        last = "left"#actualisation de la dernière jambe
        distance += 1#actualisation de la distance parcourue
        x_pos += 2  # Ajuster la valeur pour contrôler la vitesse de déplacement
        x_bot -= 20#Ajuster la valeur pour varier la difficulté
        canvas.coords(coureuse2, x_pos, 500)
        canvas.itemconfigure(coureuse1, state='hidden')#on cache l'image gauche
        canvas.itemconfigure(coureuse2, state='normal')#on affiche l'image droite

        for i in range(1, 4):
            line_coord["line_" + str(i)][0] = move_line(i - 1, line_coord["line_" + str(i)][0],
                                                        line_coord["line_" + str(i)][1])

    return distance, x_bot

def move_line(ligne, x, y):
    """
    Précondition : ligne est le nombre d'une ligne d'arrivée du canvas, x et y sont ses coordonnées
    Postcondition et role : cette fonction fait bouger et affiche la ligne d'arrivée, elle renvoie  
    les coordonnées de la ligne
    """
    global distance, finish, lines
    if finish[ligne] != True:#si la ligne d'arrivée est loin
        finish = last_meter(distance, ligne, x, y)
        if finish[ligne] == True:
            x = 1200
            canvas.coords(lines[ligne], x, y, x + 5, y + 65 * 3)
    else:#si la ligne d'arrivée est proche
        finish_line()
    background(distance)#animation du fond
    x -= 30#modification des coordonnées de la ligne d'arrivée
    if x < 0 and finish[ligne] == False:
        x = 1000
    canvas.coords(lines[ligne], x, y, x + 5, y + 60)
    return x

def background(d):
    """cette procédure fait bouger l'arrière plan, d est un entier naturel"""
    global stadium, xstade, ystade
    if d % 4 == 0:# si la distance est divisible par 4
        xstade -= 1
        canvas.coords(stadium, xstade, ystade)# modification des coordonnées du stade
        
# fonction de detection

def last_meter(d, ligne, x, y):
    """ cette fonction teste la distance à la ligne d'arrivée et renvoie une variable True ou False"""
    global finish
    if d >= 120:
        finish[ligne] = None
    if d >= 140:#affichage de la ligne d'arrivée
        finish[ligne] = True

    return finish

def finish_line():
    """cette fonction detecte quand le joueur passe la ligne d'arrivée"""
    global x_pos, line_coord
    if x_pos >= line_coord["line_1"][0]:
        print("fin du jeu")
        end_game()

def rejouer():
    """ fonction en cours de développement"""
    return True

def end_game():
    """cette procédure met fin au jeu et affiche les résultats"""
    global x_pos, x_bot, winner, chrono
    #affichage de l'écran de fin de jeu
    ecran_fin = Canvas(screen, width=600, height=400, bg="black")
    label = Label(screen,
                  text="FIN DU JEU",
                  font=("Arial", 33),
                  width=16,
                  background="black",
                  foreground="white"
                  )
    ecran_fin.place(x=250, y=100)
    label.place(x=340, y=150)
    # désactivation des commandes
    for key in screen.bind():
        screen.unbind(key)
    if x_pos > x_bot:
        ecran_fin.create_image(290, 250, image=winner)
    else:
        perdu = Label(screen, text="PERDU", font=('Arial', 44), foreground='red', background='black')
        perdu.place(x=450, y=300)
    #sauvegarde du temps
    temps = save()
    #affichage des résultats
    if chrono[0] == 0 and chrono[1] == 0 and chrono[2] <= 9 and chrono[3] < 57:
        affiche = Label(screen,
                        text="VOUS AVEZ BATTU LE RECORD ! ............." + temps,
                        font=('Arial', 16),
                        foreground='white',
                        background='black')
        affiche.place(x=300, y=450)
    else:
        affiche = Label(screen,
                        text="VOTRE TEMPS : ............." + temps,
                        font=('Arial', 16),
                        foreground='white',
                        background='black')
        affiche.place(x=350, y=450)
        arreter()
###ouverture des gifs

#gif du robot 
gif_path1 = "coureur_bot.gif"
gif_frames1 = load_gif_frames(gif_path1)

#gif du joueur
gif_path2 = "coureuse.gif"
gif_frames2 = load_gif_frames(gif_path2)

#création du canvas
canvas = Canvas(screen, width=1000, height=600)
canvas.place(x=50, y=50)
#background
stadium = canvas.create_image(xstade, ystade, image=bg)
canvas.create_image(410, 540, image=piste)
#création des lignes avec une liste en compréhention (je sais c cool)
lines = [canvas.create_rectangle(
    line_coord["line_" + str(i)][0],
    line_coord["line_" + str(i)][1],
    line_coord["line_" + str(i)][0] + 5,
    line_coord["line_" + str(i)][1] + 65,
    fill="#FFE9E6", outline="#FFE9E6") for i in range(1, 4)]

#création du bot
bot = canvas.create_image(x_bot, 385, image=gif_frames1[0])

#création des images gauches et droites de la coureuse
coureuse1 = canvas.create_image(x_pos, 500, image=gif_frames2[3])
coureuse2 = canvas.create_image(x_pos, 500, image=gif_frames2[4])
#on cache une des images pour éviter une superposition
canvas.itemconfigure(coureuse2, state='hidden')

#on anime le bot
animate_bot(canvas, gif_frames1, 0, bot)

mylabel = Label(screen, text="0 : 00 : 00 : 00", font=('Arial', 33), background='red', borderwidth=3)
mylabel.place(x=400, y=5)
#on démarre le chrono
tim()
#on active les touches s et d pour avancer
screen.bind("<d>", move_left)
screen.bind("<s>", move_right)
#boucle de fenetre
screen.mainloop()