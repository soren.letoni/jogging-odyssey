from tkinter import*
import tkinter as tk
from PIL import Image, ImageTk
import subprocess

#creation de la fenêtre 'screen'

screen = tk.Tk()
screen.geometry("1000x800")#dimension de la fenêtre
screen.title("JoggingOdyssey")#titre du jeu

###gestion des images importées

img= Image.open("piste1.png")
fond=ImageTk.PhotoImage(img)

def affiche_course_de_haie():
    '''
    Rôle : Cette fonction permet de créer une interface d'avant jeu
    '''
    global course_de_haie, mylabel, play, menu, course_100m, regle, retour
    #destruction de l'interface précédente
    course_de_haie.destroy()
    course_100m.destroy()
    #création de tout les boutons de l'interface
    mylabel= Label(canvasjeu,text="course de haie", font= ("Arial",35), bg="blue",fg="white")
    mylabel.place(x=100,y=5)
    play = Button(canvasjeu,text="PLAY",font= ("Arial",25), bg='white',activebackground="cyan",fg='blue',activeforeground="white",command=lancer_jeu_de_haie)
    play.place(x=200,y=120)
    menu = Button(canvasjeu,text="↳Menu",font= ("Arial",15),bg='red',activebackground='red',command=retour_menu)
    menu.place(x=5,y=5)
    regle = Button(canvasjeu,text="Règles et Fonctionnement de la course",font= ("Arial",15), bg='white',fg="#1EDA27",command=texte_course_de_haie)
    regle.place(x=90,y=280)
    
def affiche_course_100m():
    '''
    Rôle : Cette fonction permet de créer une interface d'avant jeu
    '''
    global course_de_haie,mylabel, play, menu, course_100m, regle, retour
    #destruction de l'interface précédente
    course_100m.destroy()
    course_de_haie.destroy()
    #création de tout les boutons de l'interface
    mylabel= Label(canvasjeu,text="course 100m", font= ("Arial",35), bg="blue",fg="white")
    mylabel.place(x=120,y=5)
    play = Button(canvasjeu,text="PLAY",font= ("Arial",25), bg='white',activebackground="cyan",fg='blue',activeforeground="white",command=lancer_course1OOm)
    play.place(x=200,y=120)
    menu = Button(canvasjeu,text="↳Menu",font= ("Arial",15),bg='red',activebackground='red',command=retour_menu)
    menu.place(x=5,y=5)
    regle = Button(canvasjeu,text="Règles et Fonctionnement de la course",font= ("Arial",15), bg='white',fg="#1EDA27",command=texte_course_100m)
    regle.place(x=90,y=280)
    
def retour_menu():
    '''
    Rôle : Cette fonction permet de revenir à l'interface de choix du jeu
    '''
    global course_de_haie, mylabel, play, menu, course_100m, regle, retour
    #création de tout les boutons de l'interface
    course_de_haie = Button(canvasjeu,text="Course de Haies",font= ("Arial",35), bg="white", foreground='blue',command=affiche_course_de_haie)
    course_de_haie.place(x=70,y=50)
    course_100m = Button(canvasjeu,text="Course 100m",font= ("Arial",35), bg="white", foreground='blue',command=affiche_course_100m)
    course_100m.place(x=100,y=170)
    #destruction de l'interface précédente
    mylabel.destroy()
    play.destroy()
    menu.destroy()
    regle.destroy()
    
def texte_course_de_haie():
    '''
    Rôle : Cette fonction permet d'afficher les règles de la course de haie
    '''
    global mylabel, play, menu, regle, retour, regle_haie1, regle_haie2, regle_haie3, regle_haie4, regle_haie5, regle_haie6, regle_haie7, regle_haie8
    #destruction de l'interface précédente
    mylabel.destroy()
    play.destroy()
    menu.destroy()
    regle.destroy()
    #création du texte et du bouton pour retouner à l'interface d'avant jeu
    retour = Button(canvasjeu,text="↳Retour",font= ("Arial",15),bg='red',activebackground='red',command=detruire_retour_haie)
    retour.place(x=5,y=5)
    regle_haie1 = Label(canvasjeu,text="-Le but de cette course est de battre l'ordinateur.",font= ("Arial",15), bg="blue",fg="white")
    regle_haie2 = Label(canvasjeu,text="-Appuyez sur \"s\" pour sauter au dessus des haies.",font= ("Arial",15), bg="blue",fg="white")
    regle_haie3 = Label(canvasjeu,text="-Faites le au niveau du triangle sur le rectangle bleu",font= ("Arial",15), bg="blue",fg="white")
    regle_haie4 = Label(canvasjeu,text="et vous ferez un \"perfect\" et gagnerez en vitesse.",font= ("Arial",15), bg="blue",fg="white")
    regle_haie5 = Label(canvasjeu,text="-Aucun effet sera appliqué avec un \"nice\" si vous sautez",font= ("Arial",15), bg="blue",fg="white")
    regle_haie6 = Label(canvasjeu,text="sur le rectangle bleu en excluant le triangle.",font= ("Arial",15), bg="blue",fg="white")
    regle_haie7 = Label(canvasjeu,text="-Enfin si vous ne sautez pas vous renverserez",font= ("Arial",15), bg="blue",fg="white")
    regle_haie8 = Label(canvasjeu,text="la haie et vous perdrez de la vitesse.",font= ("Arial",15), bg="blue",fg="white")
    regle_haie1.place(x=5,y=55)
    regle_haie2.place(x=5,y=85)
    regle_haie3.place(x=5,y=115)
    regle_haie4.place(x=5,y=145)
    regle_haie5.place(x=5,y=175)
    regle_haie6.place(x=5,y=205)
    regle_haie7.place(x=5,y=235)
    regle_haie8.place(x=5,y=265)
    
def texte_course_100m():
    '''
    Rôle : Cette fonction permet d'afficher les règles de la course de haie
    '''
    global mylabel, play, menu, regle, retour, regle_100m1, regle_100m2, regle_100m3, regle_100m4, regle_100m5, regle_100m6
    #destruction de l'interface précédente
    mylabel.destroy()
    play.destroy()
    menu.destroy()
    regle.destroy()
    #création du texte et du bouton pour retouner à l'interface d'avant jeu
    retour = Button(canvasjeu,text="↳Retour",font= ("Arial",15),bg='red',activebackground='red',command=detruire_retour_100m)
    retour.place(x=5,y=5)
    regle_100m1 = Label(canvasjeu,text="-Le but de cette course est de battre l'ordinateur.",font= ("Arial",15), bg="blue",fg="white")
    regle_100m2 = Label(canvasjeu,text="-Appuyez successivement sur \"d\" et \"s\" pour avancer.",font= ("Arial",15), bg="blue",fg="white")
    regle_100m3 = Label(canvasjeu,text="-Essayez de faire en dessous de 9s58 pour battre",font= ("Arial",15), bg="blue",fg="white")
    regle_100m4 = Label(canvasjeu,text="le record du monde!",font= ("Arial",15), bg="blue",fg="white")
    regle_100m5 = Label(canvasjeu,text="-Vous devez finir la course pour arrêter",font= ("Arial",15), bg="blue",fg="white")
    regle_100m6 = Label(canvasjeu,text="le jeu et avoir le résultat de la course.",font= ("Arial",15), bg="blue",fg="white")
    regle_100m1.place(x=5,y=55)
    regle_100m2.place(x=5,y=105)
    regle_100m3.place(x=5,y=155)
    regle_100m4.place(x=5,y=185)
    regle_100m5.place(x=5,y=235)
    regle_100m6.place(x=5,y=265)

def detruire_retour_haie():
    '''
    Fonction intermédiaire permettant de détruire l'interface de règle et fonctionnement avant de retourner à l'interfacr d'avant jeu
    '''
    global retour, regle_haie1, regle_haie2, regle_haie3, regle_haie4, regle_haie5, regle_haie6, regle_haie7, regle_haie8
    #destruction de l'interface de règle et fonctionnement
    retour.destroy()
    regle_haie1.destroy()
    regle_haie2.destroy()
    regle_haie3.destroy()
    regle_haie4.destroy()
    regle_haie5.destroy()
    regle_haie6.destroy()
    regle_haie7.destroy()
    regle_haie8.destroy()
    #appelle la fonction "affiche_course_de_haie()" pour retourner sur l'interface d'avant jeu
    affiche_course_de_haie()
    
def detruire_retour_100m():
    global retour, regle_100m1, regle_100m2, regle_100m3, regle_100m4, regle_100m5, regle_100m6
    '''
    Fonction intermédiaire permettant de détruire l'interface de règle et fonctionnement avant de retourner à l'interfacr d'avant jeu
    '''
    #destruction de l'interface de règle et fonctionnement
    retour.destroy()
    regle_100m1.destroy()
    regle_100m2.destroy()
    regle_100m3.destroy()
    regle_100m4.destroy()
    regle_100m5.destroy()
    regle_100m6.destroy()
    #appelle la fonction "affiche_course_100m()" pour retourner sur l'interface d'avant jeu
    affiche_course_100m()
    
def lancer_jeu_de_haie():
    '''
    Rôle : Cette fonction permet d'executer le programme python 'jeu de haie.py', ce qui lance le jeu de haie
    '''
    subprocess.call(['python', 'jeu de haie.py'])
    
def lancer_course1OOm():
    '''
    Rôle : Cette fonction permet d'executer le programme python 'ecrandebutcourse100.py', ce qui lance la course de 100 mètre
    '''
    subprocess.call(['python', 'ecrandebutcourse100.py'])

def animate_gif_menu(canvas, gif_frames, current_frame,gif):
    """fonction qui anime le gif en enlevant la précedante image et
    en affichant celle d'après"""
    canvas.delete(gif)
    gif = canvas.create_image(100, 385, image=gif_frames[current_frame])
    current_frame = (current_frame + 1) % len(gif_frames)
    canvas.coords(gif, 100, 385)
    screen.after(100, animate_gif_menu, canvas, gif_frames, current_frame, gif)
    
def load_gif_frames(gif_path: str) -> list:
    """
    charge les images composant gif_path
    retourne une liste des images
    """
    gif = Image.open(gif_path)#ouverture du gif
    images = []
    for i in range(gif.n_frames):# copie de chaque image du gif
        img = gif.copy()
        img.save("course" + str(i) + ".png")
        images.append(ImageTk.PhotoImage(img))
        if i < gif.n_frames - 1:
            gif.seek(gif.tell() + 1)
    return images#on renvoie la liste d'images

#création du canevas  
canvas=tk.Canvas(screen, width=1100, height=800)
canvas.pack()

canvas.create_image(400,400,image=fond)#met l'image impotée en fond d'écran

#création d'un deuxième canevas qui sera l'espace du menu interactif
canvasjeu=tk.Canvas(screen, width=500, height=333, bg="blue")
canvasjeu.place(x=400, y=250)

#création du bouton course de haie pour pouvoir aller à l'interface d'avant jeu
course_de_haie = Button(canvasjeu,text="Course de Haies",font= ("Arial",35), bg="white", fg='blue',command=affiche_course_de_haie)
course_de_haie.place(x=70,y=50)

#création du bouton course 100m pour pouvoir aller à l'interface d'avant jeu
course_100m = Button(canvasjeu,text="Course 100m",font= ("Arial",35), bg="white", fg="blue",activebackground="cyan", activeforeground="white",command=affiche_course_100m)
course_100m.place(x=100,y=170)

titre= canvas.create_text(550,100,text="Jogging Odyssey",font= ("Arial",80), fill="white")#affiche le nom du jeu

gif_path1 = "coureur_menu.gif"
gif_frames1 = load_gif_frames(gif_path1)

coureur_menu = canvas.create_image(100, 385, image=gif_frames1[0])
animate_gif_menu(canvas, gif_frames1, 0, coureur_menu)

screen.mainloop()