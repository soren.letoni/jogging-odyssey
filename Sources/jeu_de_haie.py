#importation des modules
from tkinter import *
from PIL import Image, ImageTk

#creation de la fenêtre 'screen'

screen = Tk()
screen.geometry("1100x800")#dimension de la fenêtre
screen.title("Jogging Odyssey")#titre du jeu

### mise en place des variables globales###

##variables de temps

chrono=[0,0,0,0]#initialisation du chronomètre
arret = True#permet d'arrêter le chrono

##initialisation des coordonnées

#coordonnées et pas du robot
bot_coord={'x' : 100, 'y' : 390, 'pas' : 9}

#coordonnées et pas du joueur
player_coord={ 'x' : 100, 'y' : 500, 'pas' : 7}

#coordonnées et pas du stade
stade_coord={"y" : 300, "x" : 582, "pas" : 0.5}

#coordonnées de la ligne d'arrivée
line=[1300, 395]

#coordonnées et pas des haies
haie_coord = {"haie_1": [880, 565],
              "haie_2": [800, 390], # coordonnées de la ligne 1
              "haie_3": [820, 455], # coordonnées de la ligne 2
              "haie_4": [840, 522],  # coordonnées de la ligne 3
              }

##variables d'animation

#variable de l'état du joueur/bot qui est de "course" ou "saut"
current_gif="course"
current_bot_gif="course"

#variable qui indique que le personnage ne saute pas
frames=[0,0]
frames_bot=[0,0]


# état des haies

fall=True#la haie tombe
nb_haie=0#nombre de haies passées

###gestion des images importées

img = Image.open("haie_krita.png")
haie = ImageTk.PhotoImage(img)

img = Image.open("zone.png")
jump_img = ImageTk.PhotoImage(img)
#image d'arrière plan (stade et gradins)
img = Image.open("stade.jpg")
bg = ImageTk.PhotoImage(img)

#image de victoire si le joueur gagne
img = Image.open("medaille_or.png")
winner = ImageTk.PhotoImage(img)

#image de piste d'athlétisme
img = Image.open("coursepiste.jpg")
piste = ImageTk.PhotoImage(img)

#image de haie tombée
img = Image.open("haie_fall.png")
down_haie = ImageTk.PhotoImage(img)


### fonctions de temps
def tim():
    """
    cette procédure actualise du chronomètre
    """
    global arret
    if arret == True:#s'arrete si le jeu est terminé
        mylabel.after(10, update)

def update():
    """
    cette procédure affiche le chronomètre
    """
    global chrono#prend le chronomètre global
    #variables d'affichage des 0 supplémentaires
    O_cs = ""
    O_s = ""
    O_m = ""
    chrono[3]+=1 #ajoute une centième de seconde au chronomètre
    if chrono[3] == 100:#passe de 100 centièmes de seconde à 1 seconde 
        chrono[3] = 0
        chrono[2] += 1
    if chrono[2] == 60:#passe de 60 s à 1min
        chrono[2] = 0
        chrono[1] += 1
    if chrono[1] == 60:#passe de 60 min à 1h
        chrono[1] = 0
        chrono[0] += 1
    if chrono[3] < 10:
        O_cs = "0"
    if chrono[2] < 10:
        O_s = "0"
    if chrono[1] < 10:
        O_m = "0"
    #affichage du chronomètre
    mylabel.config(text="0" + str(chrono[0]) +" : " + O_m + str(chrono[1]) +
                   " : " + O_s + str(chrono[2]) +
                   " : " + O_cs + str(chrono[3]),
                   font=('Arial', 33))
    tim()

def arreter():
    """ cette procédure indique d'arreter le chronomètre"""
    global arret
    arret = False

def save():
    """cette fonction sauvegarde le contenu du chronomètre"""
    global chrono
    return (str(chrono[0]) + " : " + str(chrono[1]) + " : " + str(chrono[2]) + " : " + str(chrono[3]))

# fonctions d'animation

def load_gif_frames(gif_path: str) -> list:
    """
    charge les images composant gif_path
    retourne une liste des images
    """
    gif = Image.open(gif_path)#ouverture du gif
    images = []

    for i in range(gif.n_frames):# copie de chaque image du gif
        img = gif.copy()
        img.save("course" + str(i) + ".png")
        images.append(ImageTk.PhotoImage(img))
        if i < gif.n_frames - 1:
            gif.seek(gif.tell() + 1)
    return images#on renvoie la liste d'images

def animate_bot(canvas, gif1, gif2, robot ):
    '''
    Précondition : gif1 et gif2 sont des gif, robot est le nombre associé à l'image robot dans le canvas
    Postcondition et rôle : cette fonction anime le robot et actualise la position du robot, elle revoie       
    l'image actuelle d'animation
    '''
    global bot_coord, current_bot_gif, player_coord, frames_bot

    if current_bot_gif=='saut':#test de l'état du robot (saut ou course)
        gif_frames=gif1#définition du gif 
        current_frame=frames_bot[0]#définition de l'image d'animation courante
    else:
        gif_frames=gif2#définition du gif 
        current_frame=frames_bot[1]#définition de l'image d'animation courante
   
    jumping_bot(bot_coord)#appel de la fonction 
    canvas.delete(robot)#suppression de l'ancienne image d'animation
    current_frame = (current_frame + 1) % len(gif_frames)#définition de la nouvelle image d'animation
    robot = canvas.create_image(bot_coord['x'], bot_coord['y'], image=gif_frames[current_frame])# ajout de la nouvelle image d'animation
    bot_coord['x'] += bot_coord['pas']-player_coord['pas']#actualisation des coordonnées du robot
    
    if current_frame==19:#detection de la fin du saut
        current_bot_gif= 'course'
        frames_bot[0]=0
    elif current_bot_gif=='saut':
        frames_bot[0]=current_frame
    else:
        frames_bot[1]=current_frame
    canvas.coords(robot, bot_coord['x'], bot_coord['y'])
    screen.after(20, animate_bot, canvas, gif1, gif2, robot)
    
    return frames_bot
    
def animate_player(canvas, gif1, gif2, player ):
    """Précondition : gif1 et gif2 sont des gif, player est le nombre associé à l'image joueur dans le canvas
    Postcondition et rôle : cette fonction anime le joueur et actualise la position du joue, elle revoie     
    l'image actuelle d'animation
    """
    global player_coord, current_gif, frames
    if current_gif=='saut':#test de l'état du joueur (saut ou course)
        gif_frames=gif1#définition du gif 
        current_frame=frames[0]#définition de l'image d'animation courante
    else:
        gif_frames=gif2#définition du gif 
        current_frame=frames[1]#définition de l'image d'animation courante
    
    canvas.delete(player)#suppression de l'ancienne image d'animation
    current_frame = (current_frame + 1) % len(gif_frames)#definition de la nouvelle image d'animation
    player = canvas.create_image(player_coord['x'], player_coord['y'], image=gif_frames[current_frame])#création de la nouvelle image d'animation
    
    player_coord['x'] += 0.2#actualisation des coordonnées du joueur
    
    if current_frame==19:#detection de fin de saut
        current_gif= 'course'
        frames[0]=0
    elif current_gif=='saut':
        frames[0]=current_frame
    else:
        frames[1]=current_frame
    canvas.coords(player, player_coord['x'], player_coord['y'])
    screen.after(20, animate_player, canvas, gif1, gif2, player)
    return frames
    #return current_gif, current_frame

def jumping_bot(coord):
    """Précondition : coord est une liste qui donne les coordonnées du robot
    Postcondition est rôle : cette fonction teste la proximité entre le robot et une haie, elle renvoie        
    l'image du gif courant
    """
    global haie_coord, current_bot_gif
    if haie_coord['haie_2'][0]-100<=coord['x']<=haie_coord['haie_2'][0]-50:#detection de la zone de saut
        current_bot_gif='saut'# le robot saute
    return current_bot_gif

def move_haie(vit, lines, n):
    """
    Précondition : prend en paramètre vit, un nombre flotant correspondant au pas des haies, lines un 
    dictionnaire contenant des coordonnnées et n un entier correspondant à un objet du canvas
    Postcondition et Rôle : cette fonction detecte si une haie est passée parfaitement, bien ou est renversée 
    et définit en conséquence la vitesse du joueur, elle renvoie la vitesse du joueur, les coordonnées des 
    haies et le nombre de haies passées
    """
    global nb_haie, fall, player_coord, bot_coord
    #détection de la haie
    if n==4 and lines["haie_"+str(n)][0]<=player_coord['x']<=lines["haie_"+str(n)][0]+2*player_coord['pas'] and fall==True:
        canvas.itemconfigure(n-1, state='normal')#la haie renversée est affichée
        canvas.itemconfigure(n+2, state='hidden')#la haie droite est cachée
        commentaire("ECHEC", "red")#appel d'une fonction
        player_coord['pas']-=1#ralentissement du joueur
        if player_coord['pas']<5:#le ralentissement est limité à 5
            player_coord['pas']=5
    if lines["haie_"+str(n)][0]<=-100:#si la haie est hors cadre, elle revient en début de course
        lines["haie_"+str(n)][0]=1300
        
        if n==4:
            canvas.itemconfigure(n+2, state='normal')
            canvas.itemconfigure(n-1, state='hidden')
            fall=True
            nb_haie+=1#on ajoute un au nombre de haies passées
            if bot_coord['pas']<15:
                bot_coord['pas']+=1#on fait accélerer le robot au fil du temps
    else:
        lines["haie_"+str(n)][0]-=player_coord['pas']#actualisation des coordonnées des haies
    if nb_haie>=5:
        lines["haie_"+str(n)][0]=1500#mise en place de la ligne d'arrivée
        final_line(player_coord['pas'])#appel de la fonction de ligne d'arrivée
    canvas.coords(2+n, lines["haie_"+str(n)][0], lines["haie_"+str(n)][1])
    screen.after(20, move_haie, vit, lines,n)
    
    return lines, nb_haie, fall

def commentaire(message, color):
    """
    Précondition : message et color sont des chaînes de carcatère
    Rôle : Cette procédure affiche un commentaire du saut PARFAIT, NICE ou ECHEC
    """
    global comment
    canvas.delete(comment)#suppression du dernier commentaire
    comment = canvas.create_text(800,50,
                  text=message, fill=color,
                  font=("Arial", 33)
                  )#affichage du nouveau commentaire

def final_line(pas):
    """
    Précondition : pas est un float
    Rôle : cette procédure anime la ligne d'arrivée
    """
    global line, line_fin,player_coord
    line[0]-=pas/5#actualisation des coordonnées de la ligne finale
    canvas.coords(line_fin, line[0],line[1],line[0]+5,line[1]+300)
    if line[0]<=player_coord['x']:#si le joueur passe la ligne d'arrivée, le jeu est terminé
        end_game()# fonction de fin de jeu

def animate_jump(event):
    '''
    Précondition  : event est une pression de touche
    Postcondition et Role : Cette fonction detecte le saut du joueur et commente le succès de celui-ci, elle 
    renvoie le gif courant
    '''
    global current_gif,haie_coord, fall, player_coord
    current_gif="saut"#gif courant de saut
    if haie_coord["haie_4"][0]-150-player_coord['pas']*player_coord['pas']**0.5<=player_coord['x']<=haie_coord["haie_1"][0]-120-player_coord['pas']*player_coord['pas']**0.5 :
        commentaire("PERFECT", "green")#le saut est parfait
        player_coord['pas']+=2#pas accéléré
        fall=False
    elif haie_coord["haie_4"][0]-175-player_coord['pas']*player_coord['pas']**0.5<=player_coord['x']<=haie_coord["haie_1"][0]-100-player_coord['pas']*player_coord['pas']**0.5:
        commentaire("NICE", "blue")#le saut est réussi
        fall=False
        player_coord['pas']+=1#pas accéléré
    return current_gif

def jump_zone(png):
    """
    Précondition : png est une image du canvas
    Role : cette procédure anime la zone de saut en bleu
    """
    global haie_coord, player_coord
    canvas.coords(png, haie_coord["haie_4"][0]-100-player_coord['pas']*player_coord['pas']**0.5, 560)
    screen.after(20, jump_zone, png)
    
def background():
    """cette procédure anime l'arrière plan (le stade)"""
    global stadium, stade_coord
    stade_coord["x"] -= stade_coord["pas"]#modification des coordonnées du stade
    canvas.coords(stadium, stade_coord["x"], stade_coord["y"])
    screen.after(100, background)

def end_game():
    """cette procédure met fin au jeu et affiche les résultats"""
    global player_coord, x_bot, winner, chrono, stade_coord
    #affichage de l'écran de fin de jeu
    ecran_fin = Canvas(screen, width=600, height=400, bg="black")
    label = Label(screen,
                  text="FIN DU JEU",
                  font=("Arial", 33),
                  width=16,
                  background="black",
                  foreground="white"
                  )
    ecran_fin.place(x=250, y=100)
    stade_coord["pas"]=0
    label.place(x=340, y=150)
    # désactivation des commandes
    for key in screen.bind():
        screen.unbind(key)
    #définition du vainqueur
    if player_coord['x'] > bot_coord['x']:
        ecran_fin.create_image(290, 250, image=winner)
    else:
        perdu = Label(screen, text="PERDU", font=('Arial', 44), foreground='red', background='black')
        perdu.place(x=450, y=300)
    #sauvegarde du temps
    temps = save()
    #affichage des résultats
    if chrono[0] == 0 and chrono[1] == 0 and chrono[2] <= 9 and chrono[3] < 57:
        affiche = Label(screen,
                        text="VOUS AVEZ BATTU LE RECORD ! ............." + temps,
                        font=('Arial', 16),
                        foreground='white',
                        background='black')
        affiche.place(x=300, y=450)
    else:
        affiche = Label(screen,
                        text="VOTRE TEMPS : ............." + temps,
                        font=('Arial', 16),
                        foreground='white',
                        background='black')
        affiche.place(x=350, y=450)
    arreter()

#ouverture et traitement des gifs

#gif du joueur
gif_path1 = "sauteur.gif"
gif_frames1 = load_gif_frames(gif_path1)#joueur sautant

gif_frames2 = gif_frames1[0:7]#joueur courant

#gif du robot
gif_path2="bot.gif"
gif_frames3=load_gif_frames(gif_path2)#robot sautant
gif_frames4=gif_frames3[0:7]#joueur courant

#création du canevas
canvas = Canvas(screen, width=1000, height=600)
canvas.place(x=50, y=50)

###créationd des objet s du canvas

#création du stade
stadium = canvas.create_image(stade_coord["x"], stade_coord["y"], image=bg)
#création de la piste
canvas.create_image(410, 540, image=piste)
#création de la haie renversée
haie_down=canvas.create_image(haie_coord["haie_1"][1],haie_coord["haie_1"][1], image=down_haie)
#création des 3 haies debout
haies=[canvas.create_image(haie_coord["haie_"+str(i)][0], haie_coord["haie_"+str(i)][1], image=haie) for i in range(2,5)]

#on cache la haie renversée
canvas.itemconfigure(haie_down, state='hidden')

#création de la zone de saut
zone=canvas.create_image(haie_coord["haie_3"][0],haie_coord["haie_3"][1], image=jump_img)

#création de la ligne d'arrivée
line_fin=canvas.create_rectangle(line[0], line[1], line[0]+5, line[1]+300,fill="#FFE9E6", outline="#FFE9E6")

#création du robot
bot=canvas.create_image(bot_coord['x'], bot_coord['y'], image=gif_frames3[0])

#création du coureur
coureur = canvas.create_image(player_coord['x'], player_coord['y'], image=gif_frames1[0])

#création du premier commentaire de saut vide
comment = canvas.create_text(400,200,text="",font=("Arial",33),)

###appel des ofnctions d'animations

#animation du joueur
animate_player(canvas, gif_frames1, gif_frames2, coureur )

#animation du robot
animate_bot(canvas, gif_frames3, gif_frames4, bot)

#animation de la zone de saut
jump_zone(zone)

#animation du fond
background()

#animation des haies
for i in range(1,5):
    move_haie(player_coord['pas'], haie_coord, i)
mylabel = Label(screen, text="0 : 00 : 00 : 00", font=('Arial', 33), background='red', borderwidth=3)
mylabel.place(x=400, y=5)

#lancement du chronomètre
tim()

screen.bind("<s>", animate_jump)
screen.mainloop()