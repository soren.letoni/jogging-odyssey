# Jogging Odyssey

Ce projet consiste en un jeu de course et un jeu de haie réalisé en Python avec l'utilisation de la bibliothèque Tkinter pour l'interface graphique et pillow (Python Imaging Library) pour le traitement des images.


## Desciption Generale du Projet

Jogging Oddesey est un projet composé de deux mini-jeux autour de l'athlétisme. Il contient un menu qui permet d'accéder à une course de 100m et un saut de haies. 
Le jeu consiste à contrôler le déplacement d'un(e) coureur/se sur une piste, évitant les obstacles (pour le saut de haies) et essayant d'atteindre la ligne d'arrivée le plus rapidement possible (pour le 100 m).

Votre but : vaincre le robot qui vous fait face et battre le record du monde.

# Fonctionnement du jeu

Lancez menu.py
Une fois dans le menu principal de notre projet, vous pouvez selectionner le jeu de course ou le saut de haies, un menu avec les règles du jeu y est présent, une fois le bouton activé, le jeu se lance

### Règles du jeu de la course de 100m

Pour le jeu de 100 m, il faut appuyer successivement sur les touches "s" et "d" pour avancer jusqu'à la ligne d'arrivée. Attention, le robot qui vous fait face ne se laissera pas battre facilement.
Note : Vous ne pouvez pas appuyer deux fois sur la même touche mais bien alterner les touches "s" et "d" (on ne gagne pas les jeux olympiques à cloche pied)
Si votre joueur n'avance tout de même pas, assurez vous bien que la touche majuscule n'est pas activée.

### Règles du jeu du saut de haies

Pour le saut de haies, il faut appuyer sur la touche "s" pour sauter, le joueur avance tout seul jusqu'à la ligne d'arrivée. Il faut appuyer sur la touche pour sauter une fois dans la zone bleue qui indique un saut réussi, la zone plus claire (blanche) à l'interieur de la zone bleue de saut indique un saut parfait qui fait augmenter deux fois plus la vitesse du joueur q'un saut réussi.
Si le saut est raté, vous perdez de la vitesse et la haie sera renverserée

Si votre joueur n'avance tout de même pas, assurez vous bien que la touche majuscule n'est pas activée. 

## Fonctionnalités

- Animation fluide de la/ du  coureuse/r sur la piste.
- Contrôle du mouvement de la/du  coureur/se
- Affichage d'un stade en arrière-plan et de lignes de délimitation sur la piste.
- Détection des collisions avec les obstacles.
- Affichage d'un message de fin de jeu lorsque la coureuse atteint la ligne d'arrivée.

## Installation et exécution

1. Assurez-vous d'avoir Python installé sur votre système.
2. Clonez ce dépôt Git sur votre machine.
3. Naviguez jusqu'au répertoire du projet.
4. Exécutez le fichier `menu.py` à l'aide de Python.

## Exigences

Ce projet nécessite l'installation des bibliothèques suivantes :

- Tkinter
- PIL

## Auteur
Ce projet a été mené par Maya Imarachen, Sören Leterrier-Santoni et Aurèle Finet en classe de première spécialité NSI au lycée Rosa Parks à la Roche Sur Yon.
